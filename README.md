# Trialday

This repo implements the trialday challenge with the following observation:

 - The challenge specifies that the server should be run via `bundle exec rackup --port 3000`
 but I couldn't find how to allow the port to be set through the params while seeting the 'app'.
 This "feature" can be found here https://github.com/rack/rack/blob/master/lib/rack/server.rb#L130-L134

First, run `bundle install`

Then run specs: `bundle exec rspec`

And run the server by executing:
```
  $ bundle exec rackup
```

Then curl a GET request:
```
  $ curl http://localhost:3000/bla -i
  HTTP/1.1 200 OK
  Content-Type: application/json
  Server: WEBrick/1.3.1 (Ruby/2.3.4/2017-03-30)
  Date: Mon, 26 Jun 2017 17:09:40 GMT
  Content-Length: 19
  Connection: Keep-Alive

  {"results":[1,2,3]}
```

or a POST request:
```
  $ curl -XPOST http://localhost:3000/bla -i -H "Content-Type: application/json" -d '{"name": "Mario"}'
  HTTP/1.1 200 OK
  Content-Type: application/json
  Server: WEBrick/1.3.1 (Ruby/2.3.4/2017-03-30)
  Date: Mon, 26 Jun 2017 17:10:16 GMT
  Content-Length: 16
  Connection: Keep-Alive

  {"name":"Mario"}
```

or an nonexistent request:
```
  $ curl http://localhost:3000/other -i
  HTTP/1.1 404 Not Found
  Content-Type: application/json
  Server: WEBrick/1.3.1 (Ruby/2.3.4/2017-03-30)
  Date: Mon, 26 Jun 2017 17:10:43 GMT
  Content-Length: 31
  Connection: Keep-Alive

  { 'error' : 'Route not found' }
```
