module Helpers
  def self.simbolize_keys(hash)
    hash.each_with_object({}) do |(k, v), hs|
      hs[k.to_sym] = v.is_a?(Hash) ? symbolize_key(v) : v
    end
  end
end
