require 'rack'
require 'json'
require './lib/helpers'

module Trialday
  GET = 'GET'
  POST = 'POST'
  VALID_REQUEST_METHODS = [GET, POST]

  class InvalidRequestMethod < StandardError; end

  @routes = Hash.new

  def self.fetch_route(method, path)
    @routes.dig(method, path)
  end

  def self.add_route(method, path, &block)
    unless VALID_REQUEST_METHODS.include?(method)
      raise InvalidRequestMethod.new("Invalid request method `#{method}`")
    end

    @routes[method] ||= {}
    @routes[method][path] = block
  end

  def self.call(env)
    request = Rack::Request.new(env)

    route = fetch_route(request.request_method, request.path_info)

    return ['404', {'Content-Type' => 'application/json'}, ["{ 'error' : 'Route not found' }"]] unless route

    if request.post?
      body = JSON.parse(request.body.read)
      body = Helpers.simbolize_keys(body)

      ['200', {'Content-Type' => 'application/json'}, [route.call(body).to_json]]
    else
      ['200', {'Content-Type' => 'application/json'}, [route.call.to_json]]
    end
  end
end

def get(path, &block)
  Trialday.add_route("GET", path, &block)
end

def post(path, &block)
  Trialday.add_route("POST", path, &block)
end
