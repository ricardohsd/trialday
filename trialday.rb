require './lib/trialday'

at_exit do
  Rack::Server.start app: Trialday, Port: 3000
end
