require 'spec_helper'
require 'stringio'
require './lib/trialday'

RSpec.describe Trialday do
  describe '#fetch_route' do
    it 'returns nil if no route is found' do
      expect(Trialday.fetch_route('GET', '/bla')).to eq(nil)
    end
  end

  describe '#add_route' do
    it 'adds a GET route and body for a given request method' do
      proc = Proc.new { { results: [1, 2, 3] } }

      Trialday.add_route('GET', '/bla', &proc)

      expect(Trialday.fetch_route('GET', '/bla')).to eq(proc)
    end

    it 'adds a POST route and body for a given request method' do
      proc = Proc.new { |name| { name: name } }

      Trialday.add_route('POST', '/bla', &proc)

      expect(Trialday.fetch_route('POST', '/bla')).to eq(proc)
    end

    it 'raises an exception when invalid request method is given' do
      proc = Proc.new { { results: [1, 2, 3] } }

      expect do
        Trialday.add_route('CS', '/bla', &proc)
      end.to raise_error('Invalid request method `CS`')
    end
  end

  describe '#get' do
    it 'adds a route and body' do
      proc = Proc.new { { results: [1, 2, 3] } }

      get('/foo', &proc)

      expect(Trialday.fetch_route('GET', '/foo')).to eq(proc)
    end
  end

  describe '#post' do
    it 'adds a route and body' do
      proc = Proc.new { |name| { name: name } }

      post('/foo', &proc)

      expect(Trialday.fetch_route('POST', '/foo')).to eq(proc)
    end
  end

  describe '#call' do
    it 'returns 404 Route not found when route doesn`t exists' do
      env = {
        'REQUEST_METHOD' => 'GET',
        'PATH_INFO' => '/test'
      }

      result = Trialday.call(env)
      expect(result).to eq(['404', {'Content-Type' => 'application/json'}, ["{ 'error' : 'Route not found' }"]])
    end

    it 'returns route GET content' do
      env = {
        'REQUEST_METHOD' => 'GET',
        'PATH_INFO' => '/some-get'
      }

      proc = Proc.new { { results: [1, 2, 3] } }

      get('/some-get', &proc)

      result = Trialday.call(env)
      expect(result).to eq(['200', {'Content-Type' => 'application/json'}, ["{\"results\":[1,2,3]}"]])
    end

    it 'returns route POST content' do
      env = {
        'REQUEST_METHOD' => 'POST',
        'PATH_INFO' => '/some-post',
        'rack.input' => StringIO.new('{"name": "Mario"}')
      }

      proc = Proc.new do |params|
        name = params[:name]

        { name: name }
      end

      post('/some-post', &proc)

      result = Trialday.call(env)
      expect(result).to eq(['200', {'Content-Type' => 'application/json'}, ["{\"name\":\"Mario\"}"]])
    end
  end
end
