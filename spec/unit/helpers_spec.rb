require 'spec_helper'
require 'stringio'
require './lib/helpers'

RSpec.describe Helpers do
  describe '#simbolize_keys' do
    it 'simbolizes a hash keys' do
      input = { 'name' => 'john', email: 'john@at.com' }

      expect(Helpers.simbolize_keys(input)).to eq({ name: 'john', email: 'john@at.com' })
    end
  end
end
